extends Control

var s = Vector2()
var ref_s = Vector2(1.4,1.4)
var check_click = 0
var size = 0
var btn 


func animate(c):
	btn = c
	check_click = 1
	size = 1.4
	pass

func _process(delta):
	size += delta
	if check_click == 1:
		s.x = size
		s.y = size
		if btn == 1 :
			$Button_meat/sprite_meat.scale = s
			#$button_sprite.scale = s
			if  s.x > 1.5 :
				check_click = 0
				$Button_meat/sprite_meat.scale = ref_s
		if btn == 2 :
			$Button_red_sauce/sprite_red_sauce.scale = s
			if  s.x > 1.5 :
				check_click = 0
				$Button_red_sauce/sprite_red_sauce.scale = ref_s
		if btn == 3 :
			$Button_white_sauce/sprite_white_sauce.scale = s
			if  s.x > 1.5 :
				check_click = 0
				$Button_white_sauce/sprite_white_sauce.scale = ref_s
		if btn == 4 :
			$Button_tomato/sprite_tomato.scale = s
			if  s.x > 1.5 :
				check_click = 0
				$Button_tomato/sprite_tomato.scale = ref_s
		if btn == 5 :
			$Button_vegetable/sprite_vegetable.scale = s
			if  s.x > 1.5 :
				check_click = 0
				$Button_vegetable/sprite_vegetable.scale = ref_s
		if btn == 6 :
			$Button_drink/sprite_drink.scale = s
			if  s.x > 1.5 :
				check_click = 0
				$Button_drink/sprite_drink.scale = ref_s
	pass
