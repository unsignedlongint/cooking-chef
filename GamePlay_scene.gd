extends Node2D

const meat_scene = preload("res://Meat.tscn")
const drink_scene = preload("res://Drink.tscn")
const dish_scene = preload("res://Dish.tscn")
const menu_scene = preload("res://Menu.tscn")
const NPC01_scene = preload("res://NPC01.tscn")
const NPC02_scene = preload("res://NPC02.tscn")
const NPC03_scene = preload("res://NPC03.tscn")
const NPC04_scene = preload("res://NPC04.tscn")
const NPC05_scene = preload("res://NPC05.tscn")
const NPC06_scene = preload("res://NPC06.tscn")
const NPC07_scene = preload("res://NPC07.tscn")
var meats = [null,null,null,null]
var dishes = [null,null,null,null]
var menus = [null,null,null,null]
var drinks = [null,null,null]
var npcs = [null,null,null,null]
var check_char_npc = [0,0,0,0,0,0,0]
var menu_list = [0,0,0,0,0,0,0,0]
var menu_q = []
var check_state_meat
var check_sidedish
var check_menu_list
var check_drink_progress = 1
var check_drink_member = 0
var drink_time = 0
var wait_drink_create
var npc_time = 0
var wait_npc_create
var posit_npc
var posit_npc_remove
var found_obj
var type
var randNPC
var rand_menu
var menu_number
var tmp
var score = 0
var add_score
var end_state = 0
var game_time
var i

#func _ready เป็น function ที่ทำงานรอบเดียวคือตอนโหลด scene นี้
func _ready():
	game_time = MySingleton.get_gmae_time()
	wait_npc_create = MySingleton.get_wait_npc_create()
	wait_drink_create = MySingleton.get_wait_drink_create()
	npc_time = 3
	drink_time = 2

#func _process เป็น function ที่จะวน loop การทำงานตลอดที่ scene นี้ยังคงอยู่
func _process(delta):
	if game_time > 0 :
		check_show_menu()
	else :
		get_tree().change_scene("res://End_game.tscn")

#func _on_Timer_timeout เป็น function ที่จะทำงานเมื่อถึงเวลาที่กำหนด ในที่นี้ตั้งให้ทำงานทุก 0.05 วินาที
func _on_Timer_timeout():
	npc_time += 0.05
	drink_time += 0.05
	game_time -= 0.05
	$bar/time_box/Label_time.text = "%d"%game_time
	# get_node("bar/time_box/Label_time").text ="%d"%game_time
	create_NPC()
	create_drink()
	drink_progresss()


func _input(event):
	#เช็คการคลิกครั้งเดียว
	if event is InputEventMouseButton:
		if event.button_index == 1 && event.pressed:
			found_obj = get_obj_at(event.position)
			if found_obj == null:
				return
			#เช็คว่า object ที่เจอเป็น type อะไร 1=meat 2=dish 3=drink
			if type == 1 :
				check_state_meat = found_obj.now_state()
				if check_state_meat == 1:
					type = 0
					$AddMeat.play()
					create_dish(i)
			elif type == 2:
				$GetBowl.play()
				check_q_order(type,found_obj)
			elif type == 3:
				$GetGlass.play()
				check_q_order(type,found_obj)
	#เช็คการ double click
	if event is InputEventMouseButton and event.doubleclick:
		if event.button_index == 1 && event.pressed:
			found_obj = get_obj_at(event.position)
			if found_obj == null:
				return
			if type == 1 :
				check_state_meat = found_obj.now_state()
				if check_state_meat == 2:
					found_obj.queue_free()
					meats[i] = null
					add_score(7)
			elif type == 2 :
				found_obj.queue_free()
				dishes[i] = null
				add_score(7)


#ดูว่าตอนมี input คลิกโดนอะไร
func get_obj_at(position):
	i = 0
	for find_meat in meats:
		if meats[i] != null :
			if find_meat.has_point(position):
				type = 1
				return find_meat
		i = i + 1
	i = 0
	for find_dash in dishes:
		if dishes[i] != null :
			if find_dash.has_point(position):
				type = 2
				return find_dash
		i = i + 1
	i = 0
	for find_drink in drinks:
		if drinks[i] != null :
			if find_drink.has_point(position):
				type = 3
				return find_drink
		i = i + 1
	type = 0
	return null

func create_meat():
	for i in range(4):
		if meats[i] == null :
			var meat = meat_scene.instance()
			meat.set_position(i + 1)
			self.add_child(meat)
			meats[i] = meat
			$BBQBegin.play()
			return
	$BtnM.play()


func create_drink():
	if drink_time > wait_drink_create:
		for i in range(3):
			if drinks[i] == null :
				var drink = drink_scene.instance()
				drink.set_position(i + 1)
				self.add_child(drink)
				drinks[i] = drink
				check_drink_member += 1
				drink_time = 0
				return

#drink_progresss แสดงแถบโหลดรอน้ำแก้วใหม่
func drink_progresss():
	if check_drink_progress == 1 :
		$Control/Button_drink/sprite_progress/TextureProgress.show()
		if drink_time < wait_drink_create && check_drink_member < 3:
			$Control/Button_drink/sprite_progress/TextureProgress.value = drink_time*(100/wait_drink_create)
		else :
			$Control/Button_drink/sprite_progress/TextureProgress.hide()
			check_drink_progress == 0

func create_dish(meat_i):
	for i in range(4):
		if dishes[i] == null :
			found_obj.queue_free()
			meats[meat_i] = null
			var dish = dish_scene.instance()
			dish.set_position(i + 1)
			self.add_child(dish)
			dishes[i] = dish
			return

func create_NPC():
	if npc_time >= wait_npc_create :
		posit_npc = random_posit_npn()
		if posit_npc != 0 :
			randNPC = random_cherecter_npn()
			if randNPC == 1 :
				var npc = NPC01_scene.instance()
				npc.set_pos(posit_npc,1)
				self.add_child(npc)
				npcs[posit_npc-1] = npc
			elif randNPC == 2 :
				var npc = NPC02_scene.instance()
				npc.set_pos(posit_npc,2)
				self.add_child(npc)
				npcs[posit_npc-1] = npc
			elif randNPC == 3 :
				var npc = NPC03_scene.instance()
				npc.set_pos(posit_npc,3)
				self.add_child(npc)
				npcs[posit_npc-1] = npc
			elif randNPC == 4 :
				var npc = NPC04_scene.instance()
				npc.set_pos(posit_npc,4)
				self.add_child(npc)
				npcs[posit_npc-1] = npc
			elif randNPC == 5 :
				var npc = NPC05_scene.instance()
				npc.set_pos(posit_npc,5)
				self.add_child(npc)
				npcs[posit_npc-1] = npc
			elif randNPC == 6 :
				var npc = NPC06_scene.instance()
				npc.set_pos(posit_npc,5)
				self.add_child(npc)
				npcs[posit_npc-1] = npc
			elif randNPC == 7 :
				var npc = NPC07_scene.instance()
				npc.set_pos(posit_npc,5)
				self.add_child(npc)
				npcs[posit_npc-1] = npc

			check_char_npc[randNPC-1] = 1
			create_menu_box(posit_npc)
			npc_time = 0

func random_posit_npn():
	for i in range(4):
		#มีอย่างน้อย 1 position ที่ว่าง
		if npcs[i] == null :
			for j in range (28):
#				posit_npc = randi()%4+1
				posit_npc = rand_range(15,19)
				posit_npc = int(posit_npc)
				posit_npc -= 14
				#position ที่ส่มได้ตรงกับ position ที่ว่าง
				if npcs[posit_npc-1] == null :
					return posit_npc
	posit_npc = 0
	return posit_npc

func random_cherecter_npn():
	for i in range(7):
		tmp = check_char_npc[i]
		#มีอย่างน้อย 1 cherecter ที่ยังไม่ได้ใช้
		if tmp == 0:
			for j in range (40):
				randNPC = rand_range(10,17)
				randNPC = int(randNPC)
				randNPC -= 9
				tmp = check_char_npc[randNPC-1]
				#cherecter ที่สุ่มได้เป็น cherecter ที่ยังไม่ได้ใช้
				if tmp == 0:
					return randNPC
	randNPC = -1
	return randNPC

#สร้าง object กล่องใส่รูปอาหารที่ npc คนนั้นสั่ง
func create_menu_box(posit_menu):
	#random_menu จะ set ค่าเมนูใน menu_list และ munu_q ให้ตรงกับ position ที่ส่งมา
	random_menu(posit_menu)
	var menu = menu_scene.instance()
	self.add_child(menu)
	menus[posit_menu-1] = menu
	menus[posit_menu-1].set_show_dish(posit_menu,menu_list)

func check_show_menu():
	for i in range(4) :
		if npcs[i] != null :
			#เช็คว่าคนเดินถึงไหนแล้ว
			tmp = npcs[i].now_state()
			#ถ้าคนยังเดินถึงจุดที่กำหนดให้โชว์ Meno box
			if tmp == 1 :
				menus[i].set_position(i+1)
			#คนเดินออกจากจุดที่กำหนดโชว์เก็บ Meno box
			elif tmp == 2 :
				destroy_menu_obj(i)
			#คนเดินออกนอกฉากแล้ว ทำลาย Menu box
			elif tmp == 3 :
				destroy_npc_obj(i)

#สุ่มเมนูที่ npc คนนั้นสั่ง
func random_menu(posit_menu):
	for i in range(2):
		rand_menu = rand_range(20,26)
		rand_menu = int(rand_menu)
		rand_menu -= 19
		tmp = posit_menu*2
		menu_list[tmp-i-1] = rand_menu
		menu_q.push_back (posit_menu)
		menu_q.push_back (rand_menu)

#เช็คว่า อาหารที่คลิกโดนมีอยู่ในคิวไหม
func check_q_order(type,found_obj):
	#ดูว่า object ที่คลิกโดนเป็นเมนูอะไร
	#menu_number เก็บเลขของเมนูที่ถูกคลิก
	menu_number = found_obj.check_menu_number()
	# for k วน loop เช็คค่าทั้งหมดที่มีใน Q (ใน menu_q)
	for k in range(menu_q.size()):
		#tmp เก็บเลขที่มีใน Q (ใน menu_q) ตามลำดับ
		tmp = menu_q[k]
		# k%2 != 0 จะเป็นการเช็คว่าเลขที่ tmp เก็บเป็นเลขของเมนูไม่ใช่เลข position ของคนที่สั่ง
		# tmp == menu_number เป็นการเอาเมนูใน tmp เช็คว่าตรงกับ memu_number ไหม
		if k%2 != 0 && tmp == menu_number:
			# posit_npc_remove จะเก็บ position ของคนที่สั่งเมนูที่ถูกคลิก
			posit_npc_remove = menu_q[k-1]
			# for j จะวน loop หาว่าเมนูที่คลิกได้อยู่ใน menu_list ที่เท่าไหร่
			for j in range(2):
				# npc แต่ละคนจะมี 2 เมนูที่สั่ง
				# check_menu_list จะหาเมนูของคนที่สั่งผ่าน menu_list โดยใช้ การ +j เป็นการหาเมนูที่ 2
				check_menu_list = (posit_npc_remove*2)-2+j
				# ให้ tmp เท่ากับเลขเมนูที่เก็บมาได้จาก menu_list
				tmp = menu_list[check_menu_list]
				# เช็คว่าเลขเมนูที่ได้จาก menu_list ตรงกับเลขเมน(menu_number)ที่คลิกได้ไหม
				if tmp == menu_number :
					# menus[posit_npc_remove-1] != null เช็คว่า menu box ที่โชว์ไม่ใช่ null
					if menus[posit_npc_remove-1] != null:
						# .clear_menu(j) ลบเมนูที่ทำไปแล้วใน box menu โดยอิงจาก posit_npc_remove-1 ที่เป็น position ของคนที่สั่งเมนูที่ถูกคลิก
						menus[posit_npc_remove-1].clear_menu(j)
					menu_list[check_menu_list] = 0
					found_obj.queue_free()
					# if type = 2 แปลว่าจานนั้นเสิร์ฟไปแล้ว ลบจานนั้นออกจากฉาก
					if type == 2 :
						dishes[i] = null
					# if type = 3 แปลว่าน้ำแก้วนั้นเสิร์ฟไปแล้ว ลบแก้วนั้นนั้นออกจากฉาก
					elif type == 3 :
						drinks[i] = null
						check_drink_member -= 1
					#เวลาในการสร้างน้ำ(func create_drink)จะเดินไปเรื่อยๆเมื่อน้ำครบ 3 แก้ว
					#ดังนั้นเมื่อมีการเสิร์ฟน้ำจะต้อง reset เวลารอน้ำแก้วต่อไปใหม่
					if drink_time > wait_drink_create :
						drink_time = 0
					$CoinClick.play()
					check_complete_order(j,check_menu_list)
					type = 0
					menu_q.remove (k-1)
					menu_q.remove (k-1)
					add_score(menu_number)
					return

#เช็คว่า NPC นั้นได้เมนูครบไหมถ้าได้ครบ .complete_order() จะ set ค่า state = 2 เพื่อให้คนเดินออก
func check_complete_order(j,check_menu_list):
	if j == 0 && menu_list[check_menu_list+1] == 0:
		npcs[posit_npc_remove-1].complete_order()
	elif j == 1 && menu_list[check_menu_list-1] == 0:
		npcs[posit_npc_remove-1].complete_order()

#check state เท่ากับ 3 คนเดินออกฉากไปแล้ว ทำลาย object
func destroy_npc_obj(destroy):
	#ทำลาย object NPC
	if npcs[destroy] != null:
		if npcs.size() == 4 :
			npc_time = 0
		tmp = npcs[destroy].check_char()
		npcs[destroy].queue_free()
		npcs[destroy] = null
		check_char_npc[tmp-1] = 0

#check state เท่ากับ 2 คนเริ่มเดินออกฉาก ทำลาย object
func destroy_menu_obj(destroy):
	#ทำลาย object menu
	if menus[destroy] != null :
		menus[destroy].queue_free()
		menus[destroy] = null
		tmp = menu_q.size()-1
		while tmp >= 0 :
			if menu_q[tmp] == destroy+1 && tmp%2 == 0:
				menu_q.remove (tmp)
				menu_q.remove (tmp)
			tmp -= 1

func add_score(menu_number) :
	match menu_number:
		1: #ขาย น้ำ
			add_score = 9
		2: #ขาย สเต็ก
			add_score = 17
		3,5: #ขาย สเต็ก + ผัก
			add_score = 28
		4,6: #ขาย สเต็ก + ผัก + ซอส
			add_score = 42
		7: #ทิ้งเนื้อที่ไหม้หรือจานอาหาร
			add_score = -15
	score += add_score
	if score < 0 :
		score = 0
	$bar/Label_score_score.text = "%d"%score
	MySingleton.set_score(score)

#---------function ชอง button-------------

func _on_Button_meat_pressed():
	$Control.animate(1)
	create_meat()

func _on_Button_red_sauce_pressed():
	$Control.animate(2)
	for find_dash in dishes:
		if find_dash != null :
			check_sidedish = find_dash.check_sidedish()
			if check_sidedish[0] == 0 && check_sidedish[1] == 0 && check_sidedish[3] == 0: 
				find_dash.add_red_sauce()
				$Addsauce.play()
				return
	$BtnM.play()

func _on_Button_white_sauce_pressed():
	$Control.animate(3)
	for find_dash in dishes:
		if find_dash != null :
			check_sidedish = find_dash.check_sidedish()
			if check_sidedish[1] == 0 && check_sidedish[0] == 0 && check_sidedish[2] == 0: 
				find_dash.add_white_sauce()
				$Addsauce.play()
				return
	$BtnM.play()

func _on_Button_tomato_pressed():
	$Control.animate(4)
	for find_dash in dishes:
		if find_dash != null :
			check_sidedish = find_dash.check_sidedish()
			if check_sidedish[2] == 0 && check_sidedish[3] == 0 && check_sidedish[1] == 0: 
				find_dash.add_tomato()
				$AddVeg.play()
				return
	$BtnM.play()

func _on_Button_vegetable_pressed():
	$Control.animate(5)
	for find_dash in dishes:
		if find_dash != null :
			check_sidedish = find_dash.check_sidedish()
			if check_sidedish[3] == 0 && check_sidedish[2] == 0 && check_sidedish[0] == 0:  
				find_dash.add_vegetable()
				$AddVeg.play()
				return
	$BtnM.play()

func _on_Button_drink_pressed():
	$Control.animate(6)
	$Control/Button_drink/sprite_drink
	$BtnM.play()
