extends Node2D
const dish_scene = preload("res://Dish.tscn")
var check_loop
var one_time_loop = 0
var state = 0
var posit_dish
var menu_list = []
var dishs = []
var wait_time
var menu_time = 0
var check_time = 100
var menu

func _init():
	wait_time = MySingleton.get_wait_time_npc()
	position.x = -200
	position.y = 280

func _ready():
	pass

func _process(delta):
	pass

func _on_Timer_timeout():
	menu_time += 0.05
	check_time = 100 - (menu_time*(100/wait_time))
	if check_time >= 66 :
		$TextureProgress1.value = check_time
	elif check_time >= 33 :
		$TextureProgress1.hide()
		$TextureProgress2.value = check_time
	else :
		$TextureProgress2.hide()
		$TextureProgress3.value = check_time

func set_position(posit):
	if one_time_loop == 0 :
		if posit == 1 :
			position.x = 590
		elif posit == 2 :
			position.x = 885
		elif posit == 3 :
			position.x = 1170
		elif posit == 4 :
			position.x = 1455
		menu_time = 0
		one_time_loop = 1

func set_show_dish(posit,menu_list):
	for i in range(2):
		posit_dish = i + 1
		menu = (posit*2)-i-1
		menu = menu_list[menu]
		var dish = dish_scene.instance()
		dish.set_position_n_menu(posit_dish,menu)
		self.add_child(dish)
		dishs.append(dish)

func clear_menu(clear_menu):
	if clear_menu == 1 && dishs[1] != null && dishs[0] != null:
		dishs[0].queue_free()
		dishs[0] = null
		dishs[1].set_posit_clear_menu()
	if clear_menu == 0 && dishs[0] != null && dishs[1] != null:
		dishs[1].queue_free()
		dishs[1] = null
		dishs[0].set_posit_clear_menu()


