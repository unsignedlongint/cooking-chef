# Plantilla de Juego 'Cooking Chef' para Godot Engine #

![](https://i.imgur.com/Oxixaf4.png)

### ¿Que trae la plantilla? ###

* Creacion de NPCs nuevos cada cierto tiempo.
* Toda las mecanicas con los temporarizadores (Timers).
* Manejo de recetas.
* Mucho mas.

### ¿Como installo en mi copia de Godot? ###

* Descargar o Clonar
* Abrir Godot
* En la pantalla inicial, Seleccionar importar proyecto.
* Localiza y abre el archivo 'project.godot'.
* ¡Listo!
