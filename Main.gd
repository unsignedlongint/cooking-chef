extends Control


func _ready():
	pass

func _on_Start_pressed():
	MySingleton.set_level(0)
	get_tree().change_scene("res://GamePlay_scene.tscn")
	
func _on_Start_pro_pressed():
	MySingleton.set_level(1)
	get_tree().change_scene("res://GamePlay_scene.tscn")
	
func _on_HowToPlay_pressed():
	get_tree().change_scene("res://HowToPlay.tscn")
	pass

func _on_Quit_pressed():
	get_tree().quit()
	
