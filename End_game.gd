extends Node2D

var score = 0

func _ready():
	score = MySingleton.get_score()
	$bg/final_score.text="%d"%score

func _on_Button_pressed():
	get_tree().change_scene("res://Main.tscn")
