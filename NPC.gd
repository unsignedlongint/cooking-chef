extends Node2D

var state = 0
var random_side
var L_side = -100
var R_side = 2020
var dst_x_posit
var speed = 7
var time = 0
var add_time = 0
var count_wait_time = 0
var num_char
var num_of_char
var wait_time
var vecter_xy = 1.4

func _init():
	wait_time = MySingleton.get_wait_time_npc()+2
	state = 0
	random_side = randi() % 2 + 1
	if random_side == 1:
		set_scale(Vector2(-vecter_xy,vecter_xy))
		position.x = L_side
		position.y = 355
	else :
		set_scale(Vector2(vecter_xy,vecter_xy))
		position.x = R_side
		position.y = 355

func _ready():
	pass

func _process(delta):
	if random_side == 1 :
		#คนกำลังเดินมาที่ position 
		if position.x < dst_x_posit :
			position.x += speed
			walk_amimation(delta)
				# NPC Completed Order
		elif state == 2:
			z_index = -5
			R_wolkout(delta)
		# NPC not Happy!
		elif count_wait_time >= wait_time:
			state = 2
			$hip/body/face2.show()
			$hip/body/face.hide()
			$hip/body/face3.hide()
			R_wolkout(delta)
		#คนถึง position ที่กำหนด
		else :
			z_index = 0
			$hip/body/face3.show()
			$hip/body/face.hide()
			count_wait_time += add_time
			add_time = 0
			state = 1

	elif random_side == 2 : 
		# NPC walking to position 
		if position.x > dst_x_posit :
			position.x -= speed
			walk_amimation(delta)
				# NPC Completed Order
		elif state == 2 :
			z_index = -1
			set_scale(Vector2(vecter_xy,vecter_xy))
			L_wolkout(delta)
		# NPC not Happy!
		elif count_wait_time >= wait_time :
			set_scale(Vector2(vecter_xy,vecter_xy))
			state = 2
			$hip/body/face2.show()
			$hip/body/face.hide()
			$hip/body/face3.hide()
			L_wolkout(delta)
		# NPC position Definition
		else :
			z_index = 1
			$hip/body/face3.show()
			$hip/body/face.hide()
			set_scale(Vector2(-vecter_xy,vecter_xy))
			count_wait_time += add_time
			add_time = 0
			state = 1

func L_wolkout(delta):
	#ให้คนเดินไปที่นอกฉาก
	if position.x > L_side :
		position.x -= speed
		walk_amimation(delta)
	#คนเดินออกนอกฉากแล้ว
	else :
		state = 3

func R_wolkout(delta):
	#ให้คนเดินไปที่นอกฉาก
	if position.x < R_side :
		position.x += speed
		walk_amimation(delta)
	#คนเดินออกนอกฉากแล้ว
	else :
		state = 3

func set_pos(pos, num_of_char):
	if pos == 1 :
		dst_x_posit = 455
	elif pos == 2 :
		dst_x_posit = 740
	elif pos == 3 :
		dst_x_posit = 1025
	elif pos == 4 :
		dst_x_posit = 1310
	num_char = num_of_char

func walk_amimation(delta):
	time += delta
	if time < 0.4 :
		$hip/body/r_arme.rotate(0.01)
		$hip/body/l_arme.rotate(-0.01)
	elif time > 0.4 && time < 0.8 :
		$hip/body/r_arme.rotate(-0.01)
		$hip/body/l_arme.rotate(0.01)
	elif time > 0.8 :
		time = 0

func now_state():
	return(state)

func check_char():
	return(num_char)

func complete_order():
	state = 2

func _on_Timer_timeout():
	if state == 1 :
		add_time += 1
	pass
