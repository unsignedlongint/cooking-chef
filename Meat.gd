extends Node2D

var meat_time = 0
var wait_time_welldone
var wait_time_scorched
var state = 0
var one_time_loop_state1 = 0
var one_time_loop_state2 = 0
var progress_welldone
var progress_scorched

func _init():
	position.x = -20
	position.y = -20
	
	wait_time_welldone = MySingleton.get_wait_time_welldone()
	wait_time_scorched = MySingleton.get_wait_time_scorched()
	progress_welldone = 100/wait_time_welldone
	progress_scorched = 100/wait_time_scorched

func _process(delta):
	meat_state()


#func meat_state แสดงเนื้อบนเตาแล้วจับเวลาว่า ดิบ/สุก/ไหม้ ใช้การเปิดปิดภาพ
func meat_state():
	if (meat_time > wait_time_welldone + wait_time_scorched):
		if one_time_loop_state2 == 0 :
			$meat_scorched.show()
			$meat_welldone.hide()
			$BBQBurnt.play()
			state = 2
			one_time_loop_state2 = 1
	elif (meat_time > wait_time_welldone):
		if one_time_loop_state1 == 0 :
			get_node("meat_welldone").show()
			$meat_welldone.show()
			$meat_raw.hide()
			state = 1
			one_time_loop_state1 = 1

#func set_positionกำหนดตำแหน่ง obj เนื้อที่สร้างใหม่ตามค่า posit ที่รับมา เลขจะเป็น 1,2,3,4 
func set_position(posit):
	if posit == 1 :
		position.x = 346
		position.y = 693
	elif posit == 2 :
		position.x = 545
		position.y = 704
	elif posit == 3 :
		position.x = 446
		position.y = 569
	elif posit == 4 :
		position.x = 625
		position.y = 577


func has_point(position):
	var rect = Rect2(-75,-40,150,80)
	var local_position = $meat_raw.to_local(position)
	return rect.has_point(local_position)

func now_state():
	return(state)

func _on_Timer_timeout():
	meat_time += 0.05
	if meat_time <= wait_time_welldone :
		$TextureProgress1.value = meat_time * progress_welldone
	elif meat_time <= wait_time_welldone + wait_time_scorched:
		$TextureProgress2.show()
		$TextureProgress1.hide()
		$TextureProgress2.value = (meat_time-wait_time_welldone) * progress_scorched
	else :
		$TextureProgress2.hide()
	pass
