extends Node2D

var check_sidedish = [0,0,0,0]

func _init():
	position.x = -20
	position.y = -20
	
func set_position(posit):
	if posit == 1 :
		position.x = 914
		position.y = 718
	elif posit == 2 :
		position.x = 1190
		position.y = 722
	if posit == 3 :
		position.x = 932
		position.y = 587
	elif posit == 4 :
		position.x = 1174
		position.y = 590

func has_point(position):
	var rect = Rect2(-115,-40,230,80)
	var local_position = $dish.to_local(position)
	return rect.has_point(local_position)

func check_sidedish():
	return(check_sidedish)

func add_red_sauce():
	$red_sause.show()
	check_sidedish[0] = 1
	pass

func add_white_sauce():
	$white_sause.show()
	check_sidedish[1] = 1
	pass

func add_tomato():
	$tomato.show()
	check_sidedish[2] = 1
	pass
	
func add_vegetable():
	$vegetable.show()
	check_sidedish[3] = 1

func check_menu_number():
	if $vegetable.is_visible() == true && $white_sause.is_visible() == true:
		return 4
	elif $vegetable.is_visible() == true:
		return 3
	elif $tomato.is_visible() == true && $red_sause.is_visible() == true:
		return 6
	elif $tomato.is_visible() == true:
		return 5
	elif $steak.is_visible() == true && $white_sause.is_visible() == false && $red_sause.is_visible() == false:
		return 2
	else :
		return 0

func set_position_n_menu(posit_dish,menu):
	set_scale(Vector2(0.60,0.60))
	position.x = 10
	#set ตำแหน่งของจาน
	if posit_dish == 1 :
		position.y = -37
	elif posit_dish == 2 :
		position.y = 37
	#set menu ของจานนั้น
	if menu == 1:
		$dish.hide()
		$steak.hide()
		$drink.show()
	elif menu == 2:
		$steak.show()
	elif menu == 3:
		$vegetable.show()
	elif menu == 4:
		$vegetable.show()
		$white_sause.show()
	elif menu == 5:
		$tomato.show()
	elif menu == 6:
		$tomato.show()
		$red_sause.show()

func set_posit_clear_menu():
	position.y = 0
	pass

